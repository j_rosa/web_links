package com.example.rosajak.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Calling the button functions
        webLinkGoogleButton();
        webLinkSAICButton();
        webLinkISSAICButton();
        webLinkYahooButton();


    }

        private void webLinkGoogleButton() {

            Button googleButton = (Button) findViewById(R.id.hyperlink);

            googleButton.setOnClickListener(new View.OnClickListener() {
                @Override

                //functionality of when the button is clicked
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                    startActivity(browserIntent);
                }
            });
    }

        private void webLinkSAICButton() {

            Button saicButton = (Button) findViewById(R.id.hyperlink2);

            saicButton.setOnClickListener(new View.OnClickListener() {
            @Override

                //functionality of when the button is clicked
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.saic.com"));
                    startActivity(browserIntent);
                }
        });
    }

        private void webLinkISSAICButton() {

            Button ISSAICButton = (Button) findViewById(R.id.hyperlink4);

            ISSAICButton.setOnClickListener(new View.OnClickListener() {
                @Override

                //functionality of when the button is clicked
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://issaic.saic.com"));
                    startActivity(browserIntent);
                }
            });
    }

        private void webLinkYahooButton() {

            Button YahooButton = (Button) findViewById(R.id.hyperlink3);

            YahooButton.setOnClickListener(new View.OnClickListener() {
                @Override

                //functionality of when the button is clicked
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.yahoo.com/"));
                    startActivity(browserIntent);
                }
            });
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
